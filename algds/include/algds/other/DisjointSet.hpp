/*
 *  Algorithms and Data Structures
 *  Copyright (C) 2022  Georgij Krajnyukov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <unordered_map>
#include <initializer_list>
#include <iterator>
#include <ostream>
#include <concepts>
#include <vector>


template<typename T>
concept DisjointSetElement = requires(T a, T b)
{
    std::unordered_map<T, T>();
    a == b; a != b;
};


template<DisjointSetElement T>
class DisjointSet
{
public:
    template<std::forward_iterator ForwardIt>
    DisjointSet(ForwardIt, ForwardIt);

    DisjointSet(size_t elements_count);
    DisjointSet(std::initializer_list<T>);
    DisjointSet& operator=(std::initializer_list<T>);

    DisjointSet(const DisjointSet&) = delete;
    DisjointSet& operator=(const DisjointSet&) = delete;

    T& find(const T&) const;
    void join(const T&, const T&);

    size_t elements_count() const;

    template<DisjointSetElement E>
    friend std::ostream& operator<<(std::ostream&, const DisjointSet<E>&);
private:
    mutable std::unordered_map<T, T> leaders;
    std::unordered_map<T, size_t> sizes;
};


template<DisjointSetElement T>
template<std::forward_iterator ForwardIt>
DisjointSet<T>::DisjointSet(ForwardIt first, ForwardIt last)
{
    for (auto it = first; it != last; ++it)
    {
        leaders.insert(std::make_pair(*it, *it));
        sizes.insert(std::make_pair(*it, 1));
    }
}


template<DisjointSetElement T>
DisjointSet<T>::DisjointSet(size_t elements_count)
{
    for (size_t i = 0; i < elements_count; ++i)
    {
        T el(i);
        leaders.insert(std::make_pair(el, el));
        sizes.insert(std::make_pair(el, 1));
    }
}


template<DisjointSetElement T>
DisjointSet<T>::DisjointSet(std::initializer_list<T> il)
{
    for (auto&& el : il)
    {
        leaders.insert(std::make_pair(el, el));
        sizes.insert(std::make_pair(el, 1));
    }
}


template<DisjointSetElement T>
DisjointSet<T>& DisjointSet<T>::operator=(std::initializer_list<T> il)
{
    for (auto&& el : il)
    {
        leaders.insert(std::make_pair(el, el));
        sizes.insert(std::make_pair(el, 1));
    }

    return *this;
}


template<DisjointSetElement T>
T& DisjointSet<T>::find(const T& el) const
{
    T& leader = leaders.at(el);
    if (leader != el)
    {
        leader = find(leader);
        leaders.insert_or_assign(el, leader);
    }
    return leader;
}


template<DisjointSetElement T>
void DisjointSet<T>::join(const T& el0, const T& el1)
{
    T leader0 = find(el0);
    T leader1 = find(el1);

    if (leader0 == leader1)
        return;
    size_t size0 = sizes.at(leader0);
    size_t size1 = sizes.at(leader1);

    if (size0 < size1)
        std::swap(leader0, leader1);

    leaders.insert_or_assign(leader1, leader0);
    sizes.insert_or_assign(leader0, size0 + size1);
}


template<DisjointSetElement T>
size_t DisjointSet<T>::elements_count() const
{
    return leaders.size();
}


template<DisjointSetElement T>
std::ostream& operator<<(std::ostream& out, const DisjointSet<T>& ds)
{
    std::unordered_map<T, std::vector<T>> sets;
    for (auto it = ds.leaders.cbegin(); it != ds.leaders.cend(); ++it)
    {
        T& leader = ds.find(it->second);
        if (!sets.contains(leader))
        {
            std::vector<T> set;
            set.reserve(ds.sizes.at(leader));
            sets.insert(std::make_pair(leader, set));
        }
        sets.at(leader).push_back(it->first);
    }

    out << '{';
    for (auto& set : sets)
    {
        out << '{';
        for (auto& el : set.second)
            out << el << ", ";
        out << "}, ";
    }
    out << '}';
    return out;
}