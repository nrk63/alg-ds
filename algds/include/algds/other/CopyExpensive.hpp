/*
 *  Algorithms and Data Structures
 *  Copyright (C) 2022  Georgij Krajnyukov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <vector>
#include <compare>
#include <ostream>

class CopyExpensive
{
public:
    CopyExpensive(unsigned int ordinal, size_t size);

    [[nodiscard]] unsigned int get_ordinal() const;
    [[nodiscard]] size_t get_size() const;
private:
    unsigned int ordinal;
    std::vector<char> bytes;
};

std::strong_ordering operator<=>(const CopyExpensive&, const CopyExpensive&);
std::ostream& operator<<(std::ostream&, const CopyExpensive&);
