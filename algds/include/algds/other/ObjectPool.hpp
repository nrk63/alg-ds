/*
 *  Algorithms and Data Structures
 *  Copyright (C) 2022  Georgij Krajnyukov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <cstddef>
#include <vector>
#include <memory>
#include <utility>

template<typename T, typename Allocator=std::allocator<T>>
class ObjectPool
{
public:
    ObjectPool(size_t size, Allocator allocator=Allocator{});

    ObjectPool(const ObjectPool&) = delete;
    ObjectPool& operator=(const ObjectPool&) = delete;

    ObjectPool(ObjectPool&&) = default;
    ObjectPool& operator=(ObjectPool&&) = default;

    virtual ~ObjectPool();

    template<typename... Args>
    std::shared_ptr<T> acquire(Args&&... args);

    bool empty() const;
private:
    const size_t size;
    Allocator allocator;
    T* first_object;
    std::vector<T*> free_objects;
};


template<typename T, typename Allocator>
ObjectPool<T, Allocator>::ObjectPool(size_t size, Allocator allocator)
    :size(size), allocator(std::move(allocator))
{
    first_object = this->allocator.allocate(size);
    free_objects.reserve(size);
    for (ptrdiff_t i = 0; i < size; ++i)
        free_objects.push_back(first_object + i);
}


template<typename T, typename Allocator>
ObjectPool<T, Allocator>::~ObjectPool()
{
    allocator.deallocate(first_object, size);
    first_object = nullptr;
    free_objects.clear();
}


template<typename T, typename Allocator>
template<typename... Args>
std::shared_ptr<T> ObjectPool<T, Allocator>::acquire(Args&&... args)
{
    if (empty())
        return nullptr;
    T* object_ptr = free_objects.back();
    free_objects.pop_back();
    new(object_ptr) T { std::forward<Args>(args)... };
    return std::shared_ptr<T> { object_ptr,
            [this](T* released_ptr)
            {
                std::destroy_at(released_ptr);
                free_objects.push_back(released_ptr);
            }};
}


template<typename T, typename Allocator>
bool ObjectPool<T, Allocator>::empty() const
{
    return free_objects.empty();
}
