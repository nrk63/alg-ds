/*
 *  Algorithms and Data Structures
 *  Copyright (C) 2022  Georgij Krajnyukov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <vector>

class TuringMachine
{
public:
    class Instruction
    {
    public:
        enum MoveTape : int
        {
            Left = -1,
            Halt,
            Right,
        };

        Instruction() = default;
        Instruction(int next_state, int write_symbol, MoveTape move_tape);

        [[nodiscard]] int get_next_state() const;
        [[nodiscard]] int get_write_symbol() const;
        [[nodiscard]] MoveTape get_move_tape() const;
    private:
        int next_state = -1;
        int write_symbol = -1;
        MoveTape move_tape = Halt;
    };

    TuringMachine(std::vector<std::vector<Instruction>> program,
                  int final_state, std::vector<int>& tape);

    bool next();
private:
    int state;
    int final_state;
    int position;
    std::vector<int>& tape;
    std::vector<std::vector<Instruction>> program;
};
