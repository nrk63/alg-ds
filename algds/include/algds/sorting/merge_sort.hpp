﻿/*
 *  Algorithms and Data Structures
 *  Copyright (C) 2022  Georgij Krajnyukov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <iterator>
#include <vector>
#include <type_traits>
#include <functional>

namespace sorting
{
template<std::forward_iterator ForwardIt, typename Comparator>
void merge_sort(ForwardIt first_it, ForwardIt last_it, Comparator comparator,
                std::vector<typename ForwardIt::value_type>& buffer)
{
    auto size = std::distance(first_it, last_it);
    if (size <= 1)
        return;

    auto middle_it = first_it;
    std::advance(middle_it, size / 2);

    merge_sort(first_it, middle_it, comparator, buffer);
    merge_sort(middle_it, last_it, comparator, buffer);

    auto left = first_it;
    auto right = middle_it;
    while (left != middle_it && right != last_it)
    {
        if (comparator(*left, *right))
            buffer.push_back(std::move(*(left++)));
        else
            buffer.push_back(std::move(*(right++)));
    }

    while (left != middle_it)
        buffer.push_back(std::move(*(left++)));

    while (right != last_it)
        buffer.push_back(std::move(*(right++)));

    for (auto&& element : buffer)
        *(first_it++) = std::move(element);
    buffer.clear();
}


template<std::forward_iterator ForwardIt,
         typename Comparator=std::less<typename ForwardIt::value_type>>
void merge_sort(ForwardIt first_it, ForwardIt last_it,
                Comparator comparator=Comparator())
{
    auto size = std::distance(first_it, last_it);
    if (size <= 1)
        return;
    std::vector<typename std::iterator_traits<ForwardIt>::value_type> buffer;
    buffer.reserve(size);
    merge_sort(first_it, last_it, comparator, buffer);
}
}
