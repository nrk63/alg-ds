/*
 *  Algorithms and Data Structures
 *  Copyright (C) 2022  Georgij Krajnyukov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <iterator>
#include <utility>

namespace sorting
{
template<std::random_access_iterator RandomIt>
void max_heapify(RandomIt array, auto heap_size, auto index)
{
    auto left = 2*index + 1;
    auto right = left + 1;
    auto largest = index;
    if (left < heap_size && array[left] > array[largest])
        largest = left;
    if (right < heap_size && array[right] > array[largest])
        largest = right;

    if (largest != index)
    {
        std::swap(array[index], array[largest]);
        max_heapify(array, heap_size, largest);
    }
}


template<std::random_access_iterator RandomIt>
void build_max_heap(RandomIt first, auto size)
{
    auto heap_size = size;
    for (auto i = heap_size/2 - 1; i >= 0; --i)
        max_heapify(first, heap_size, i);
}


template<std::random_access_iterator RandomIt>
void heapsort(RandomIt first, RandomIt last)
{
    auto size = std::distance(first, last);
    build_max_heap(first, size);
    for (auto heap_size = size - 1; heap_size >= 1; --heap_size)
    {
        std::swap(first[0], first[heap_size]);
        max_heapify(first, heap_size, 0);
    }
}
}
