/*
 *  Algorithms and Data Structures
 *  Copyright (C) 2022  Georgij Krajnyukov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <type_traits>
#include <utility>
#include <functional>
#include <iterator>

namespace sorting
{
template<std::forward_iterator ForwardIt, typename Comparator=std::less<
                                typename std::iterator_traits<ForwardIt>::value_type>>
void choice_sort(ForwardIt first, ForwardIt last, Comparator comparator=Comparator())
{
    for (; first != last; ++first)
    {
        ForwardIt choice = first;
        ForwardIt it = first;
        ++it;
        for (; it != last; ++it)
            if (comparator(*it, *choice))
                choice = it;
        std::swap(*first, *choice);
    }
}
}
