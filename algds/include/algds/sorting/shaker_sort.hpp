/*
 *  Algorithms and Data Structures
 *  Copyright (C) 2022  Georgij Krajnyukov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <iterator>
#include <type_traits>
#include <functional>
#include <utility>

namespace sorting
{
template<std::bidirectional_iterator BidirIterator, typename Comparator=std::less<
                typename std::iterator_traits<BidirIterator>::value_type>>
void shaker_sort(BidirIterator first, BidirIterator last, Comparator comparator=Comparator())
{
    bool swapped = first != last;
    while (swapped)
    {
        swapped = false;
        BidirIterator cur = first;
        BidirIterator next = first;
        ++next;
        while (next != last)
        {
            if (comparator(*next, *cur))
            {
                std::swap(*cur, *next);
                swapped = true;
            }
            ++cur;
            ++next;
        }
        if (!swapped)
            return;
        --last;

        swapped = false;
        cur = last;
        next = last;
        --cur;
        while (next != first)
        {
            if (comparator(*next, *cur))
            {
                std::swap(*cur, *next);
                swapped = true;
            }
            --cur;
            --next;
        }
        ++first;
    }
}
}
