/*
 *  Algorithms and Data Structures
 *  Copyright (C) 2022  Georgij Krajnyukov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <vector>
#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <functional>

/**
 * \brief Performs each submitted task using one of possibly
 *        several pooled threads.
 *
 * If all threads are busy, tasks will be enqueued and performed later
 * in the submit order. Enqueueing is thread-safe.
 * \line
 * All threads are joined on ThreadPool destruction.
 */
class ThreadPool
{
public:
    /**
     * \brief Constructs a new instance with specified number of threads.
     *
     * \param size number of threads to pool
     */ 
    ThreadPool(size_t size);

    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    /**
     * \brief Destructs an instance and joins all pooled threads.
     */
    virtual ~ThreadPool();

    /**
     * \brief Enqueues a task and notifies a thread.
     * 
     * \param task functional object representing the task
     */
    void perform(std::function<void()> task);

private:
    std::vector<std::thread> threads;
    std::queue<std::function<void()>> tasks;
    std::mutex tasks_mutex;

    /** \brief Condition variable that notifies threads when task is enqueued. */
    std::condition_variable task_enqueued;

    /** \brief Flag for threads to stop. */
    bool stopped = false;
};
