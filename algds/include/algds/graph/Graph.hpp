/*
 *  Algorithms and Data Structures
 *  Copyright (C) 2022  Georgij Krajnyukov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <vector>
#include <initializer_list>
#include <ostream>
#include <concepts>
#include <tuple>
#include <set>
#include <numeric>
#include <tuple>
#include <iostream>
#include <utility>
#include <complex>
#include <forward_list>
#include <functional>
#include <iterator>
#include <type_traits>

#include "../other/DisjointSet.hpp"


using VertexOrdinal = size_t;
using EdgeOrdinal = size_t;
using WeightType = double;


class Edge
{
public:
    Edge(EdgeOrdinal, VertexOrdinal, VertexOrdinal, WeightType);

    [[nodiscard]] EdgeOrdinal get_ordinal() const;
    [[nodiscard]] VertexOrdinal get_first() const;
    [[nodiscard]] VertexOrdinal get_second() const;
    [[nodiscard]] WeightType get_weight() const;
private:
    EdgeOrdinal ordinal;
    VertexOrdinal first;
    VertexOrdinal second;
    WeightType weight;
};


template <typename T>
class Vertex
{
public:
    Vertex() = default;

    Vertex(VertexOrdinal, const T& value);
    Vertex(VertexOrdinal, T&& value);

    [[nodiscard]] VertexOrdinal get_ordinal() const;
    [[nodiscard]] T& get_value();
    [[nodiscard]] const T& get_value() const;
    [[nodiscard]] const std::vector<Edge>& get_adjacent_vertices() const;

    void set_value(const T&);

    void add_edge(const Edge&);
    void add_edge(Edge&&);
private:
    VertexOrdinal ordinal {};
    T value;
    std::vector<Edge> adjacent_vertices;
};


template<typename T>
class Graph
{
public:
    template<bool Const>
    class CommonIterator
    {
    private:
        using GraphPointer = typename std::conditional<Const, const Graph<T>*, Graph<T>*>::type;
    public:
        using difference_type = std::ptrdiff_t;
        using value_type = typename std::conditional<Const, const Vertex<T>, Vertex<T>>::type;
        using reference_type = typename std::conditional<Const, const Vertex<T>&, Vertex<T>&>::type;
        using pointer_type = typename std::conditional<Const, const Vertex<T>*, Vertex<T>*>::type;
        using iterator_category = std::bidirectional_iterator_tag;

        CommonIterator() = default;
        explicit CommonIterator(GraphPointer g=nullptr, VertexOrdinal v=VertexOrdinal(0U));

        reference_type operator*() const;
        pointer_type operator->() const;

        CommonIterator& operator++();
        CommonIterator operator++(int);

        CommonIterator& operator--();
        CommonIterator operator--(int);

        bool operator==(const CommonIterator&) const noexcept;
        bool operator!=(const CommonIterator&) const noexcept;
    private:
        GraphPointer graph = nullptr;
        VertexOrdinal vertex_ordinal {};
    };

    using iterator = CommonIterator<false>;
    using const_iterator = CommonIterator<true>;

    using NamelessEdge = std::tuple<VertexOrdinal, VertexOrdinal, WeightType>;

    const_iterator cbegin() const;
    const_iterator cend() const;
    const_iterator crbegin() const;
    const_iterator crend() const;

    const_iterator begin() const;
    const_iterator end() const;
    iterator begin();
    iterator end();
    iterator rbegin();
    iterator rend();

    Graph() = default;
    explicit Graph(VertexOrdinal vertices_count);

    Graph(std::initializer_list<Edge>);
    Graph& operator=(std::initializer_list<Edge>);

    Graph(std::initializer_list<NamelessEdge>);
    Graph& operator=(std::initializer_list<NamelessEdge>);

    Vertex<T>& operator[](VertexOrdinal);
    const Vertex<T>& operator[](VertexOrdinal) const;

    void emplace_vertex(const T&);
    void emplace_vertex(T&&);

    void add_vertex(const Vertex<T>&);
    void add_vertex(Vertex<T>&&);

    void emplace_directed_edge(VertexOrdinal, VertexOrdinal, WeightType);
    void add_directed_edge(const Edge&);

    void emplace_edge(VertexOrdinal, VertexOrdinal, WeightType);
    void add_edge(const Edge&);

    [[nodiscard]] VertexOrdinal vertices_count() const;
    [[nodiscard]] EdgeOrdinal edges_count() const;
    [[nodiscard]] WeightType get_weight() const;
private:
    std::vector<Vertex<T>> vertices;
    size_t next_vertex_ordinal = 0U;
    size_t next_edge_ordinal = 0U;
};

namespace graph
{
template<typename T>
[[nodiscard]] Graph<T> mst_kruskal(const Graph<T>&);

template<typename T>
[[nodiscard]] Graph<T> mst_prim(const Graph<T>&, VertexOrdinal);

template<typename T>
[[nodiscard]] std::vector<Graph<T>> dijkstra(const Graph<T>&, VertexOrdinal);

template<typename T>
[[nodiscard]] Graph<T> shortest_paths_dynamic(const Graph<T>& graph,
                                              VertexOrdinal from,
                                              VertexOrdinal to);

template<typename T>
void _shortest_paths_dynamic(const Graph<T>& graph, VertexOrdinal tbd_vertex_ordinal,
                             std::vector<WeightType>& f, std::vector<VertexOrdinal>& parent);

template<typename T>
typename Graph<T>::CommonIterator dfs(std::function<bool(const Vertex<T>&)> matcher,
                                      const Graph<T>&);

template<typename T>
typename Graph<T>::Iterator _dfs(std::function<bool(const Vertex<T>&)> matcher,
                                 const Graph<T>&, const Vertex<T>&, std::vector<bool>& used);
}


Edge::Edge(EdgeOrdinal ordinal, VertexOrdinal first, VertexOrdinal second, WeightType weight)
    :ordinal(ordinal), first(first), second(second), weight(weight) {}


EdgeOrdinal Edge::get_ordinal() const
{
    return ordinal;
}


VertexOrdinal Edge::get_first() const
{
    return first;
}


VertexOrdinal Edge::get_second() const
{
    return second;
}


WeightType Edge::get_weight() const
{
    return weight;
}


template<typename T>
Vertex<T>::Vertex(VertexOrdinal ordinal, const T& value)
    :ordinal(ordinal), value(value) {}


template<typename T>
Vertex<T>::Vertex(VertexOrdinal ordinal, T&& value)
    :ordinal(ordinal), value(std::move(value)) {}


template<typename T>
VertexOrdinal Vertex<T>::get_ordinal() const
{
    return ordinal;
}


template<typename T>
T& Vertex<T>::get_value()
{
    return value;
}


template<typename T>
const T& Vertex<T>::get_value() const
{
    return value;
}


template<typename T>
const std::vector<Edge>& Vertex<T>::get_adjacent_vertices() const
{
    return adjacent_vertices;
}


template<typename T>
void Vertex<T>::set_value(const T& value)
{
    this->value = value;
}


template<typename T>
void Vertex<T>::add_edge(const Edge& edge)
{
    adjacent_vertices.push_back(edge);
}


template<typename T>
void Vertex<T>::add_edge(Edge&& edge)
{
    adjacent_vertices.push_back(std::move(edge));
}


template<typename T>
template<bool Const>
Graph<T>::CommonIterator<Const>::CommonIterator(GraphPointer graph,
                                                VertexOrdinal vertex_ordinal)
    :graph(graph), vertex_ordinal(vertex_ordinal) {}


template<typename T>
template<bool Const>
typename Graph<T>::template CommonIterator<Const>::reference_type Graph<T>::CommonIterator<Const>::operator*() const
{
    if (vertex_ordinal >= graph->vertices_count() || graph == nullptr)
        throw std::out_of_range("invalid iterator dereference");
    return (*graph)[vertex_ordinal];
}


template<typename T>
template<bool Const>
typename Graph<T>::template CommonIterator<Const>::pointer_type Graph<T>::CommonIterator<Const>::operator->() const
{
    return &operator*();
}


template<typename T>
template<bool Const>
typename Graph<T>::template CommonIterator<Const>& Graph<T>::CommonIterator<Const>::operator++()
{
    ++vertex_ordinal;
    return *this;
}


template<typename T>
template<bool Const>
typename Graph<T>::template CommonIterator<Const> Graph<T>::CommonIterator<Const>::operator++(int)
{
    CommonIterator<Const> prev = *this;
    operator++();
    return prev;
}


template<typename T>
template<bool Const>
typename Graph<T>::template CommonIterator<Const>& Graph<T>::CommonIterator<Const>::operator--()
{
    --vertex_ordinal;
    return *this;
}


template<typename T>
template<bool Const>
typename Graph<T>::template CommonIterator<Const> Graph<T>::CommonIterator<Const>::operator--(int)
{
    CommonIterator<Const> prev = *this;
    operator--();
    return prev;
}


template<typename T>
template<bool Const>
bool Graph<T>::CommonIterator<Const>::operator==(const CommonIterator<Const>& other) const noexcept
{
    return graph == other.graph && vertex_ordinal == other.vertex_ordinal;
}


template<typename T>
template<bool Const>
bool Graph<T>::CommonIterator<Const>::operator!=(const CommonIterator<Const>& other) const noexcept
{
    return !operator==(other);
}


template<typename T>
typename Graph<T>::const_iterator Graph<T>::cbegin() const
{
    return begin();
}


template<typename T>
typename Graph<T>::const_iterator Graph<T>::cend() const
{
    return end();
}


template<typename T>
typename Graph<T>::const_iterator Graph<T>::crbegin() const
{
    return std::reverse_iterator(cbegin());
}


template<typename T>
typename Graph<T>::const_iterator Graph<T>::crend() const
{
    return std::reverse_iterator(cend());
}


template<typename T>
typename Graph<T>::const_iterator Graph<T>::begin() const
{
    return const_iterator(this);
}


template<typename T>
typename Graph<T>::const_iterator Graph<T>::end() const
{
    return const_iterator(this, vertices_count());
}


template<typename T>
typename Graph<T>::iterator Graph<T>::begin()
{
    return iterator(this);
}


template<typename T>
typename Graph<T>::iterator Graph<T>::end()
{
    return iterator(this, vertices_count());
}


template<typename T>
typename Graph<T>::iterator Graph<T>::rbegin()
{
    return std::reverse_iterator(begin());
}


template<typename T>
typename Graph<T>::iterator Graph<T>::rend()
{
    return std::reverse_iterator(end());
}


template<typename T>
Graph<T>::Graph(VertexOrdinal vertices_count)
    : vertices(vertices_count) {}


template<typename T>
Graph<T>::Graph(std::initializer_list<Edge> il)
{
    VertexOrdinal max_ordinal = 0U;
    for (auto& edge : il)
        max_ordinal = std::max({max_ordinal, edge.get_first(), edge.get_second()});

    vertices.resize(max_ordinal + 1);
    for (auto& edge : il)
        add_edge(edge);
}


template<typename T>
Graph<T>& Graph<T>::operator=(std::initializer_list<Edge> il)
{
    Graph new_graph(il);
    std::swap(*this, new_graph);
    return *this;
}


template<typename T>
Graph<T>::Graph(std::initializer_list<NamelessEdge> il)
{
    VertexOrdinal max_ordinal = 0U;
    for (auto& edge : il)
        max_ordinal = std::max({max_ordinal, std::get<0>(edge), std::get<1>(edge)});

    vertices.resize(max_ordinal + 1);
    for (auto& edge : il)
        emplace_edge(std::get<0>(edge), std::get<1>(edge), std::get<2>(edge));
}


template<typename T>
Graph<T>& Graph<T>::operator=(std::initializer_list<NamelessEdge> il)
{
    Graph new_graph(il);
    std::swap(*this, new_graph);
    return *this;
}


template<typename T>
Vertex<T>& Graph<T>::operator[](VertexOrdinal vertex_ordinal)
{
    return vertices[vertex_ordinal];
}


template<typename T>
const Vertex<T>& Graph<T>::operator[](VertexOrdinal vertex_ordinal) const
{
    return vertices[vertex_ordinal];
}


template<typename T>
void Graph<T>::emplace_vertex(const T& value)
{
    vertices.emplace_back(next_vertex_ordinal++, value);
}


template<typename T>
void Graph<T>::emplace_vertex(T&& value)
{
    vertices.emplace_back(next_vertex_ordinal++, std::move(value));
}


template<typename T>
void Graph<T>::add_vertex(const Vertex<T>& vertex)
{
    VertexOrdinal ordinal = vertex.get_ordinal();
    if (vertices.size() <= ordinal)
        vertices.resize(ordinal + 1);
    vertices[ordinal] = vertex;
}


template<typename T>
void Graph<T>::add_vertex(Vertex<T>&& vertex)
{
    VertexOrdinal ordinal = vertex.get_ordinal();
    if (vertices.size() <= ordinal)
        vertices.resize(ordinal + 1);
    vertices[ordinal] = std::move(vertex);
}


template<typename T>
void Graph<T>::emplace_directed_edge(VertexOrdinal first, VertexOrdinal second, WeightType weight)
{
    if (vertices.size() <= std::max(first, second))
        vertices.resize(std::max(first, second) + 1);
    vertices[first].add_edge(Edge(next_edge_ordinal, first, second, weight));
    ++next_edge_ordinal;
}


template<typename T>
void Graph<T>::add_directed_edge(const Edge& edge)
{
    VertexOrdinal first = edge.get_first();
    VertexOrdinal second = edge.get_second();
    if (vertices.size() <= std::max(first, second))
        vertices.resize(std::max(first, second) + 1);
    vertices[first].add_edge(Edge(next_edge_ordinal, first,
                                  second, edge.get_weight()));
    ++next_edge_ordinal;
}


template<typename T>
void Graph<T>::emplace_edge(VertexOrdinal first, VertexOrdinal second, WeightType weight)
{
    if (vertices.size() <= std::max(first, second))
        vertices.resize(std::max(first, second) + 1);
    vertices[first].add_edge(Edge(next_edge_ordinal, first, second, weight));
    vertices[second].add_edge(Edge(next_edge_ordinal, second, first, weight));
    ++next_edge_ordinal;
}


template<typename T>
void Graph<T>::add_edge(const Edge& edge)
{
    VertexOrdinal first = edge.get_first();
    VertexOrdinal second = edge.get_second();
    if (vertices.size() <= std::max(first, second))
        vertices.resize(std::max(first, second) + 1);
    vertices[first].add_edge(Edge(next_edge_ordinal, first,
                                  second, edge.get_weight()));
    vertices[second].add_edge(Edge(next_edge_ordinal, second,
                                   first, edge.get_weight()));
    ++next_edge_ordinal;
}


template<typename T>
Graph<T> graph::mst_kruskal(const Graph<T>& graph)
{
    Graph<T> result(graph.vertices_count());
    auto comparator = [](const Edge& lhs, const Edge& rhs)
    {
        if (lhs.get_weight() < rhs.get_weight())
            return true;
        if (lhs.get_weight() == rhs.get_weight())
            return lhs.get_ordinal() < rhs.get_ordinal();
        return false;
    };

    std::set<Edge, decltype(comparator)> edges(comparator);
    for (auto&& vertex : graph)
        for (auto&& edge : vertex.get_adjacent_vertices())
            edges.insert(edge);

    DisjointSet<VertexOrdinal> components(graph.vertices_count());
    for (auto& edge : edges)
    {
        VertexOrdinal first = edge.get_first();
        VertexOrdinal second = edge.get_second();

        if (components.find(first) != components.find(second))
        {
            result.add_edge(edge);
            components.join(first, second);
        }
    }

    return result;
}


template<typename T>
Graph<T> graph::mst_prim(const Graph<T>& graph, VertexOrdinal primary_vertex)
{
    VertexOrdinal vertices_count = graph.vertices_count();
    Graph<T> result(vertices_count);
    if (vertices_count < 1)
        return result;

    std::vector<WeightType> key(vertices_count, std::numeric_limits<WeightType>::max());
    std::vector<VertexOrdinal> parent(vertices_count, vertices_count + 1);

    auto priority =
    [&key](const VertexOrdinal& lhs, const VertexOrdinal& rhs)
    {
        if (key[lhs] < key[rhs])
            return true;
        if (key[lhs] == key[rhs])
            return lhs < rhs;
        return false;
    };

    key[primary_vertex] = 0.0;
    std::set<VertexOrdinal, decltype(priority)> vertices_queue(priority);
    std::vector<bool> used(vertices_count);
    vertices_queue.insert(primary_vertex);

    while (!vertices_queue.empty())
    {
        VertexOrdinal next = *vertices_queue.begin();
        vertices_queue.erase(vertices_queue.begin());

        if (parent[next] < vertices_count)
        {
            result.emplace_edge(parent[next], next, key[next]);
            used[next] = true;
        }

        for (auto&& edge : graph[next].get_adjacent_vertices())
        {
            VertexOrdinal vertex = edge.get_second();
            if (!used[vertex] && edge.get_weight() < key[vertex])
            {
                vertices_queue.erase(vertex);
                parent[vertex] = next;
                key[vertex] = edge.get_weight();
                vertices_queue.insert(vertex);
            }
        }
    }

    return result;
}


template<typename T>
std::vector<Graph<T>> graph::dijkstra(const Graph<T>& graph, VertexOrdinal primary_vertex)
{
    VertexOrdinal vertices_count = graph.vertices_count();
    std::vector<Graph<T>> result(vertices_count);
    if (vertices_count <= 1)
        return result;

    std::vector<WeightType> key(vertices_count, std::numeric_limits<WeightType>::max());
    std::vector<VertexOrdinal> parent(vertices_count, vertices_count + 1);

    auto priority =
    [&key](const VertexOrdinal& lhs, const VertexOrdinal& rhs)
    {
        if (key[lhs] < key[rhs])
            return true;
        if (key[lhs] == key[rhs])
            return lhs < rhs;
        return false;
    };

    key[primary_vertex] = 0.0;
    std::set<VertexOrdinal, decltype(priority)> vertices_queue(priority);
    vertices_queue.insert(primary_vertex);

    while (!vertices_queue.empty())
    {
        VertexOrdinal next = *vertices_queue.begin();
        vertices_queue.erase(vertices_queue.begin());

        for (auto&& edge : graph[next].get_adjacent_vertices())
        {
            VertexOrdinal vertex = edge.get_second();
            if (key[next] + edge.get_weight() < key[vertex])
            {
                vertices_queue.erase(vertex);
                parent[vertex] = next;
                key[vertex] = key[next] + edge.get_weight();
                vertices_queue.insert(vertex);
            }
        }
    }

    for (auto start = VertexOrdinal(0); start < vertices_count; ++start)
    {
        Graph<T> path(vertices_count);
        for (auto vertex = start; parent[vertex] < vertices_count; vertex = parent[vertex])
        {
            const Vertex<T>& first = graph[vertex];
            const Vertex<T>& second = graph[parent[vertex]];
            Edge min_edge(0, 0, 0, std::numeric_limits<WeightType>::max());
            for (auto&& edge : second.get_adjacent_vertices())
            {
                if (edge.get_second() == vertex)
                {
                    if (min_edge.get_second() == vertex
                        && min_edge.get_weight() > edge.get_weight())
                    {
                        min_edge = edge;
                    }
                    else if (min_edge.get_second() != vertex)
                    {
                        min_edge = edge;
                    }
                }
            }
            path[first.get_ordinal()].set_value(first.get_value());
            path[second.get_ordinal()].set_value(second.get_value());
            path.add_edge(min_edge);
        }
        result[start] = path;
    }

    return result;
}


template<typename T>
Graph<T> graph::shortest_paths_dynamic(const Graph<T>& graph,
                                       VertexOrdinal from,
                                       VertexOrdinal to)
{
    auto vertices_count = graph.vertices_count();
    std::vector<WeightType> f(vertices_count, std::numeric_limits<WeightType>::max());
    std::vector<VertexOrdinal> parent(vertices_count, vertices_count + 1);

    parent[from] = from;
    f[from] = 0.0;

    _shortest_paths_dynamic(graph, to, f, parent);

    Graph<T> result(vertices_count);
    parent[from] = vertices_count;
    for (auto vertex = to; parent[vertex] < vertices_count; vertex = parent[vertex])
    {
        const Vertex<T>& first = graph[vertex];
        const Vertex<T>& second = graph[parent[vertex]];
        Edge min_edge(0, 0, 0, std::numeric_limits<WeightType>::max());
        Edge& min = std::min(second.get_adjacent_vertices().cbegin(), second.get_adjacent_vertices().cend(),
            [](const Edge& lhs, const Edge& rhs) { return lhs.get_weight() < rhs.get_weight(); });
        for (auto&& edge : second.get_adjacent_vertices())
        {
            if (edge.get_second() == vertex)
            {
                if (min_edge.get_second() == vertex
                    && min_edge.get_weight() > edge.get_weight())
                {
                    min_edge = edge;
                }
                else if (min_edge.get_second() != vertex)
                {
                    min_edge = edge;
                }
            }
        }
        result[first.get_ordinal()].set_value(first.get_value());
        result[second.get_ordinal()].set_value(second.get_value());
        result.add_edge(min_edge);
    }

    return result;
}


template<typename T>
void graph::_shortest_paths_dynamic(const Graph<T>& graph, VertexOrdinal tbd_vertex_ordinal,
                             std::vector<WeightType>& f, std::vector<VertexOrdinal>& parent)
{
    for (const Vertex<T>& vertex : graph)
    {
        auto start_vertex_ordinal = vertex.get_ordinal();
        for (auto&& edge : vertex.get_adjacent_vertices())
        {
            auto second = graph[edge.get_second()];
            if (second.get_ordinal() != tbd_vertex_ordinal)
                continue;
            if (parent[start_vertex_ordinal] >= graph.vertices_count())
                _shortest_paths_dynamic(graph, start_vertex_ordinal, f, parent);
            if (f[tbd_vertex_ordinal] > f[start_vertex_ordinal] + edge.get_weight())
            {
                parent[tbd_vertex_ordinal] = start_vertex_ordinal;
                f[tbd_vertex_ordinal] = f[start_vertex_ordinal] + edge.get_weight();
            }
        }
    }
}


template<typename T>
VertexOrdinal Graph<T>::vertices_count() const
{
    return vertices.size();
}


template<typename T>
size_t Graph<T>::edges_count() const
{
    return next_edge_ordinal;
}


template<typename T>
WeightType Graph<T>::get_weight() const
{
    std::vector<bool> used(edges_count());
    WeightType result = 0.0;
    for (auto&& vertex : vertices)
    {
        for (auto&& edge : vertex.get_adjacent_vertices())
        {
            if (!used[edge.get_ordinal()])
            {
                used[edge.get_ordinal()] = true;
                result += edge.get_weight();
            }
        }
    }
    return result;
}


template<typename T>
typename Graph<T>::CommonIterator graph::dfs(std::function<bool(const Vertex<T>&)> matcher,
                                             const Graph<T>& graph)
{
    std::vector<bool> used(graph.vertices_count());
    for (auto&& vertex : graph)
    {
        auto it = dfs(matcher, vertex, used);
        if (it != graph.end())
            return it;
    }
    return graph.end();
}


template<typename T>
typename Graph<T>::Iterator graph::_dfs(std::function<bool(const Vertex<T>&)> matcher,
                                 const Graph<T>& graph, const Vertex<T>& vertex,
                                 std::vector<bool>& used)
{
    if (matcher(vertex))
        return graph[vertex.get_ordinal()];
    used[vertex.get_ordinal()] = true;

    for (auto&& edge : vertex.get_adjacent_vertices())
    {
        if (!used[edge.get_second()])
        {
            auto&& it = dfs(matcher, graph[edge.get_second()], used);
            if (it != graph.end())
                return it;
        }
    }
    return graph.end();
}


std::ostream& operator<<(std::ostream& out, const Edge& edge)
{
    return out << "Edge(" << edge.get_ordinal() << ", "
               << edge.get_first() << ", " << edge.get_second()
               << ", " << edge.get_weight() << ')';
}


template<typename T>
std::ostream& operator<<(std::ostream& out, const Vertex<T>& vertex)
{
    return out << "Vertex(" << vertex.get_ordinal() << ", "
               << vertex.get_value() << ')';
}


template<typename T>
std::ostream& operator<<(std::ostream& out, const Graph<T>& graph)
{
    out << "Graph{";
    std::vector<bool> used(graph.vertices_count(), false);
    for (auto&& vertex : graph)
    {
        for (auto&& edge : vertex.get_adjacent_vertices())
        {
            if (!used[edge.get_ordinal()])
            {
                used[edge.get_ordinal()] = true;
                out << edge << ", ";
            }
        }
    }
    return out << '}';
}
