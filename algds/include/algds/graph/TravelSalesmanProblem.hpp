/*
 *  Algorithms and Data Structures
 *  Copyright (C) 2022  Georgij Krajnyukov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <vector>

class TravelSalesmanProblem
{
public:
    void solve(const std::vector<std::vector<double>>& adjacency);

    [[nodiscard]] double get_result_weight() const;
    [[nodiscard]] std::vector<int> get_result() const;
private:
    void branch(const std::vector<std::vector<double>>& adjacency,
                int to_vertex, double current_weight);

    double upper_bound;
    std::vector<bool> used;
    std::vector<int> path;
    std::vector<int> result;
};
