/*
 *  Algorithms and Data Structures
 *  Copyright (C) 2022  Georgij Krajnyukov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <algds/other/CopyExpensive.hpp>

#include <vector>
#include <compare>
#include <ostream>

CopyExpensive::CopyExpensive(unsigned int ordinal, size_t size)
    :ordinal(ordinal), bytes(size) {}


unsigned int CopyExpensive::get_ordinal() const
{
    return ordinal;
}


size_t CopyExpensive::get_size() const
{
    return bytes.size();
}


std::strong_ordering operator<=>(const CopyExpensive& lhs, const CopyExpensive& rhs)
{
    return lhs.get_ordinal() <=> rhs.get_ordinal();
}


std::ostream& operator<<(std::ostream& out, const CopyExpensive& target)
{
    return out << "CopyExpensive(" << target.get_ordinal() << ", "
               << target.get_size() << ')';
}
