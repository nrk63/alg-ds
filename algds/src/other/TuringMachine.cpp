/*
 *  Algorithms and Data Structures
 *  Copyright (C) 2022  Georgij Krajnyukov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <algds/other/TuringMachine.hpp>

#include <utility>


TuringMachine::Instruction::Instruction(int next_state, int write_symbol,
                                        MoveTape move_tape)
    :next_state(next_state), write_symbol(write_symbol), move_tape(move_tape) {}


int TuringMachine::Instruction::get_next_state() const
{
    return next_state;
}


int TuringMachine::Instruction::get_write_symbol() const
{
    return write_symbol;
}


TuringMachine::Instruction::MoveTape TuringMachine::Instruction::get_move_tape() const
{
    return move_tape;
}


TuringMachine::TuringMachine(std::vector<std::vector<Instruction>> program,
                             int final_state, std::vector<int>& tape)
    :program(std::move(program)), state(0), final_state(final_state),
     tape(tape), position(0) {}


bool TuringMachine::next()
{
    if (state == final_state)
        return false;
    Instruction instruction = program[state][tape[position]];
    tape[position] = instruction.get_write_symbol();
    position += static_cast<int>(instruction.get_move_tape());
    state = instruction.get_next_state();
    return true;
}
