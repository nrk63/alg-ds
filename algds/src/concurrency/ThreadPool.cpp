/*
 *  Algorithms and Data Structures
 *  Copyright (C) 2022  Georgij Krajnyukov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <algds/concurrency/ThreadPool.hpp>

#include <vector>
#include <thread>
#include <functional>

ThreadPool::ThreadPool(size_t size)
{
    for (size_t i = 0; i < size; ++i)
    {
        threads.emplace_back(
            [this]()
            {
                while (true)
                {
                    std::unique_lock lock(tasks_mutex);
                    task_enqueued.wait(lock, [this] { return !tasks.empty() || stopped; });
                    if (stopped)
                        return;

                    auto task = tasks.front();
                    tasks.pop();
                    lock.unlock();

                    task();
                }
            });
    }
}


ThreadPool::~ThreadPool()
{
    stopped = true;
    task_enqueued.notify_all();
    for (auto&& thread : threads)
        thread.join();
}


void ThreadPool::perform(std::function<void()> task)
{
    std::unique_lock lock(tasks_mutex);
    tasks.push(std::move(task));
    task_enqueued.notify_one();
}
