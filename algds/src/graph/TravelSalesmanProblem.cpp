/*
 *  Algorithms and Data Structures
 *  Copyright (C) 2022  Georgij Krajnyukov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <algds/graph/TravelSalesmanProblem.hpp>

#include <numeric>

void TravelSalesmanProblem::solve(const std::vector<std::vector<double>>& adjacency)
{
    upper_bound = std::numeric_limits<double>::max();
    path.clear();
    path.reserve(adjacency.size());
    used = std::vector<bool>(adjacency.size(), false);
    for (int start_vertex = 0; start_vertex < adjacency.size(); ++start_vertex)
    {
        path.push_back(start_vertex);
        for (int to_vertex = 0; to_vertex < adjacency.size(); ++to_vertex)
        {
            if(adjacency[start_vertex][to_vertex] > 0.0)
                branch(adjacency, to_vertex, adjacency[start_vertex][to_vertex]);
        }
        path.pop_back();
    }
}


double TravelSalesmanProblem::get_result_weight() const
{
    return upper_bound;
}


std::vector<int> TravelSalesmanProblem::get_result() const
{
    return result;
}


void TravelSalesmanProblem::branch(const std::vector<std::vector<double>>& adjacency,
                                   int to_vertex, double current_weight)
{
    if (to_vertex == path.front() && path.size() == adjacency.size())
    {
        upper_bound = current_weight;
        result = path;
        result.push_back(to_vertex);
        return;
    }

    path.push_back(to_vertex);
    used[to_vertex] = true;
    for (int u = 0; u < adjacency.size(); ++u)
    {
        if(!used[u] && adjacency[to_vertex][u] > 0.0)
        {
            if (current_weight + adjacency[to_vertex][u] < upper_bound)
                branch(adjacency, u, current_weight + adjacency[to_vertex][u]);
        }
    }
    path.pop_back();
    used[to_vertex] = false;
}
