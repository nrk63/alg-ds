/*
 *  Algorithms and Data Structures
 *  Copyright (C) 2022  Georgij Krajnyukov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <algds/other/TuringMachine.hpp>

#include <iostream>

int main()
{
    using MoveTape = TuringMachine::Instruction::MoveTape;

    int BLANK = 2;
    int ASTERISK = 3;
    int TMP0 = 4;
    int TMP1 = 5;

    //Copy 0, 1 sequence after a separator
    std::vector<std::vector<TuringMachine::Instruction>> program = {
        //State 0
        //Moving until 0, 1
        {
            {1, 0, MoveTape::Right},
            {1, 1, MoveTape::Right},
            {0, BLANK, MoveTape::Right},
        },
        //State 1
        //Moving right until blank symbol to replace it with asterisk
        {
            {1, 0, MoveTape::Right},
            {1, 1, MoveTape::Right},
            //blank
            {2, ASTERISK, MoveTape::Left},
        },
        //State 2
        //Moving left until not 0, 1 or *
        {
            {2, 0, MoveTape::Left},
            {2, 1, MoveTape::Left},
            //blank
            {3, BLANK, MoveTape::Right},
            //asterisk
            {2, ASTERISK, MoveTape::Left},
            //tmp symbol for 0
            {3, TMP0, MoveTape::Right},
            //tmp symbol for 1
            {3, TMP1, MoveTape::Right}
        },
        //State 3
        //What to write: 0 or 1
        {
            {4, TMP0, MoveTape::Right},
            {5, TMP1, MoveTape::Right},
            //blank: never
            {},
            //asterisk: pointing that copy was made
            {6, ASTERISK, MoveTape::Left},
        },
        //State 4
        //Moving right until blank to write 0
        {
            {4, 0, MoveTape::Right},
            {4, 1, MoveTape::Right},
            //blank
            {2, 0, MoveTape::Left},
            //asterisk
            {4, ASTERISK, MoveTape::Right},
        },
        //State 5
        //Moving right until blank to write 1
        {
            {5, 0, MoveTape::Right},
            {5, 1, MoveTape::Right},
            //blank
            {2, 1, MoveTape::Left},
            //asterisk
            {5, ASTERISK, MoveTape::Right},
        },
        //State 6
        //Moving left until blank to replace tmp symbols
        {
            {6, 0, MoveTape::Left},
            {6, 1, MoveTape::Left},
            //blank: end
            {7, BLANK, MoveTape::Halt},
            //asterisk: never
            {},
            {6, 0, MoveTape::Left},
            {6, 1, MoveTape::Left}
        }
    };
    int FINAL_STATE = 7;

    std::vector<int> tape = {2, 2, 2, 0, 1, 0, 0, 1, 2,
                                      2, 2, 2, 2, 2,
                             2, 2, 2, 2};

    TuringMachine tm(program, FINAL_STATE, tape);
    while (tm.next());
    for (int symbol : tape)
        std::cout << symbol << " ";
    std::cout << std::endl;

    return 0;
}
