/*
 *  Algorithms and Data Structures
 *  Copyright (C) 2022  Georgij Krajnyukov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <random>
#include <chrono>

#include <algds/concurrency/ThreadPool.hpp>

static const size_t POOL_SIZE = 6;
static const size_t DATASET_SIZE = 20'000;
static const double MEAN_VALUE = 2480.0;
static const double DEVIATION = 0.95;

void center(std::vector<double>& dataset)
{
    double mean = 0.0;
    for (double x : dataset)
        mean += x;
    mean /= dataset.size();

    for (auto it = dataset.begin(); it != dataset.end(); ++it)
        *it -= mean;
}

int main()
{
    using namespace std::chrono;

    std::vector<std::vector<double>> datasets(2 * POOL_SIZE, std::vector<double>(DATASET_SIZE));
    std::random_device seeder;
    const auto seed = seeder.entropy() ? seeder() : time(nullptr);
    std::mt19937 rnd_engine(static_cast<std::mt19937::result_type>(seed));
    std::normal_distribution dist(MEAN_VALUE, DEVIATION);

    for (auto&& dataset : datasets)
    {
        std::generate(dataset.begin(), dataset.end(),
            [&rnd_engine, &dist]() { return dist(rnd_engine); });
    }

    auto start = high_resolution_clock::now();
    ThreadPool pool(POOL_SIZE);
    for (auto&& dataset : datasets)
        pool.perform([&dataset]() { center(dataset); });
    auto end = high_resolution_clock::now();
    std::cout << "Thread pool runned in " << duration_cast<milliseconds>(end - start).count()
        << "ms" << std::endl;

    start = high_resolution_clock::now();
    for (auto&& dataset : datasets)
    {
        std::jthread thread{ [&dataset]() { center(dataset); } };
    }
    end = high_resolution_clock::now();
    std::cout << "jthreads runned in " << duration_cast<milliseconds>(end - start).count()
        << "ms" << std::endl;

    return 0;
}
