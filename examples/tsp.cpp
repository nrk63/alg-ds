/*
 *  Algorithms and Data Structures
 *  Copyright (C) 2022  Georgij Krajnyukov
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <vector>
#include <algds/graph/TravelSalesmanProblem.hpp>

int main()
{
    std::vector<std::vector<double>> adj = {
        {0, 15, -1, 5, 13},
        {15, 0, 5, 4, 7},
        {-1, 5, 0, 3, -1},
        {5, 4, 3, 0, 2},
        {13, 7, -1, 2, 0}
    };

    TravelSalesmanProblem tsp;
    tsp.solve(adj);
    std::vector<int> result = tsp.get_result();
    std::cout << tsp.get_result_weight() << std::endl;
    for (int v : result)
        std::cout << v << " ";
    std::cout << std::endl;

	return 0;
}

